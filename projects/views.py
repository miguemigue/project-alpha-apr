from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm
from projects.models import Project

# Create your views here.


@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects": projects,
    }
    return render(request, "projects/list.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            projects = form.save(commit=False)
            projects.owner = request.user
            projects.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
        context = {
            "form": form,
        }
    return render(request, "projects/create.html", context)


@login_required
def show_project(request, id):
    projects = Project.objects.filter(owner=request.user)
    projects = get_object_or_404(Project, id=id)
    context = {
        "projects": projects,
    }
    return render(request, "projects/detail.html", context)
